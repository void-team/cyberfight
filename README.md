# CyberFight

A Multiplayer version of the Mech Fight battlegame from [CyberBreak](https://gitlab.com/void-team/cyberbreak)

This game introduces the (fighting) branch of Anarchy Battle System that'll be seen in Origins.

## Anarchy Battle System (ABS)

Anarchy Battle System (Internal name "ABS", Common name "Anarchy") is a battle system that thinks of battles a bit more differently than most other systems.

Instead of grinding EXP to level up, like in other games, Anarchy does some math to pinpoint your skill level.

There are 2 different catergories for a level in this branch, an Offense Catergory and a Defense Catergory.

In the Offense catergory, Anarchy takes the Amount of damage per attack and divides it by the Max Amount of damage possible per that attack. It does this for every attack type possible, so that Anarchy can give you suggestions on which attacks you need to work on. 

In the Defense catergory, Anarchy does the same, it grabs the amount of damage taken per attack that either gets close to hitting the player or hits them, and divides it by the max amount of damage possible. It also does this for every attack type possible, and gives the player suggestions on which attacks need improvement on dodging/defending. 

In the end, ABS reports these findings to me so that I can buff and debuff attacks if needed. 

With ABS also introduces the concept of Anarchy Battles. Anarchy battles occur when not enough players of a balanced skill level are online, or a player's win streak and Anarchy Levels are just too high. Instead of there being a free for all or a 2v2v2 battle, It tries to balance the power levels by bending the bounds of "fair". It'll break apart regular teams and create "balanced" ones, like a 5v1 or a 4v1v1 or a 3v2v1 etc etc. It'll also put together teams where 1 person weighs down the other ones, just to see how resilient people can be.
